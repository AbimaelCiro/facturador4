@php
$path_style = app_path('CoreFacturalo'.DIRECTORY_SEPARATOR.'Templates'.DIRECTORY_SEPARATOR.'pdf'.DIRECTORY_SEPARATOR.'style.css');
@endphp

<head>
    <link href="{{ $path_style }}" rel="stylesheet" />
</head>
<br/>
<table class="full-width">
        <tr>
            <td class="border-bottom"></td>
        </tr>
    </table>
<body>

    <table class="full-width">
        <tr>
            <td class="text-center desc font-bold">Para consultar el comprobante ingresar a {!! url('/buscar') !!}</td>
        </tr>
    </table>
</body>